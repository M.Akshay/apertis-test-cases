#!/usr/bin/python3
###################################################################################
# Apertis LAVA QA tool wrapper
# Copyright (C) 2021 Collabora Ltd
# Detlev Casanova <detlev.casanova@collabora.com>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import argparse
import json
import subprocess

from urllib.error import URLError
from urllib.request import urlopen

parser = argparse.ArgumentParser(description="submit LAVA jobs for Apertis")
parser.add_argument('--jobid-file', type=argparse.FileType("w"), help="output file for the list of created job ids")
parser.add_argument('--metadata-file', type=argparse.FileType("r"),
                    help="set the metadata file location")
parser.add_argument('-u', '--callback-url', type=str,
                    help="specify the callback URL")
parser.add_argument('--callback-secret', type=str,
                    help="specify the secret token to use for the callback")
args, unknown = parser.parse_known_args()

if args.metadata_file and (not args.callback_url or not args.callback_secret):
    print("--callback-url and --callback-secret must be set with --metadata-file")
    exit(1)

# Run lqa with the given arguments
result = subprocess.run(['lqa'] + unknown, stderr=subprocess.PIPE)

if result.returncode != 0:
    print(f"lqa returned an error: {result.returncode}")
    exit(1)

submitted = []
for job in result.stderr.decode('utf-8').splitlines():
    # Add Job ID to the list
    try:
        job_id = int(job.split(' ')[-1])
    except:
        print(job)
        continue

    submitted.append(job_id)

    if args.metadata_file:
        # Load metadata for phabbridge
        try:
            metadata = json.load(args.metadata_file)
        except Exception as e:
            print(f"Cannot load metadata file {e}")
            continue

        # Notify the phab bridge about the job
        request = {
            'status_string': 'submitted',
            'metadata': metadata,
            'id': job_id,
            'token': args.callback_secret
        }
        try:
            urlopen(args.callback_url, data=json.dumps(request).encode('UTF-8'))
        except URLError:
            pass

if args.jobid_file:
    json.dump({ "jobids": submitted }, args.jobid_file)
