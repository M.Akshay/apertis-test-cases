metadata:
  name: sanity-check
  format: "Apertis Test Definition 1.0"
  image-types:
    fixedfunction: [ armhf, arm64, amd64 ]
    hmi:           [ armhf, amd64 ]
    basesdk:       [ amd64 ]
    sdk:           [ amd64 ]
  image-deployment:
    - APT
    - OSTree
  type: sanity
  exec-type: automated
  priority: critical
  maintainer: "Apertis Project"
  description: "This test checks that the system bus is up, that a user session is
                created and that a graphical interface is successfully running in
                the system, as well as other similar basic services so that a
                stable and reliable environment can be confirmed before futher
                tests are executed.
                Basic functionality: services."

  pre-conditions:
    - "A second system with a terminal and git available on it is required in order
       to run the above steps."
    - "This test must be executed before other tests and even before installing
       other packages in the image, so the script to execute this test should be
       fetched from a different machine."
    - "Since the Apertis image runs sshd by default, you can easily copy this file
       using the scp utility from the second machine, for this you will need the
       Apertis image IP, which you can find running the ip command from Apertis:"
    - $ ip addr
    - "Alternatively, you can copy the apertis-tests directory in other ways, for
       example by using an external storage device, manually mounting the device
       and copying the folder. It is recommended and expected that you use scp
       from a second machine."
    - "Once the apertis-tests folder is available in the Apertis image, follow the
       execution steps."

  expected:
    - "The command should report no failure and its output should be something
       like this at the end:"
    - |
        >+ grep -E ^NAME=("?)Apertis\1$ /etc/os-release
        NAME="Apertis"
        + grep -E ^ID=("?)apertis\1$ /etc/os-release
        ID=apertis
        + id -u user
        + test -S /run/user/1000/wayland-0
        + set +x
        # Sanity check successful

  notes:
    - "IMPORTANT: If this test fails for an image, NO further tests should be
       executed on that image, since this invalidates all test results."
    - "This test must be executed in an image before running either automated or
       manual tests."

install:
  git-repos:
    - url: https://gitlab.apertis.org/pkg/apertis-tests.git
      branch: 'apertis/v2023dev1'

run:
  steps:
    - "# Enter test directory:"
    - cd apertis-tests
    - "# Execute the following command:"
    - common/sanity-check

parse:
  pattern: 'TEST_RESULT:(?P<result>\w+):(?P<test_case_id>[^:]+):'
